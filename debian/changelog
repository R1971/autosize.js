autosize.js (4.0.4~dfsg1+~4.0.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + libjs-autosize: Drop versioned constraint on ruby-rails-assets-autosize in
      Breaks.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 17:57:39 +0000

autosize.js (4.0.4~dfsg1+~4.0.0-1) unstable; urgency=medium

  * Team upload
  * Update standards version to 4.6.0, no changes needed.
  * Drop nodejs dependency
  * Embed typescript declarations
  * New upstream version 4.0.4~dfsg1+~4.0.0

 -- Yadd <yadd@debian.org>  Fri, 10 Dec 2021 18:12:07 +0100

autosize.js (4.0.2~dfsg1-7) unstable; urgency=medium

  * Switch to rollup for generating umd instead of babel (Closes: #990458)

 -- Pirate Praveen <praveen@debian.org>  Sat, 03 Jul 2021 22:35:42 +0530

autosize.js (4.0.2~dfsg1-6) experimental; urgency=medium

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Modernize debian/watch
  * Fix GitHub tags regex
  * Use dh-sequence-nodejs

  [ Pirate Praveen ]
  * Update minimum version of node-babel-plugin-add-module-exports (>= 1.0.2~)
    (Closes: #990458)

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Jun 2021 22:41:44 +0530

autosize.js (4.0.2~dfsg1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + libjs-autosize: Add Multi-Arch: foreign.
    + node-autosize: Add Multi-Arch: foreign.

  [ Pirate Praveen ]
  * Use add-module-exports babel plugin to build (Closes: #977304)
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 14 Dec 2020 17:42:45 +0530

autosize.js (4.0.2~dfsg1-4) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Xavier Guimard ]
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Build with babel7
  * Use pkg-js-tools auto install

 -- Xavier Guimard <yadd@debian.org>  Mon, 11 May 2020 11:01:16 +0200

autosize.js (4.0.2~dfsg1-3) unstable; urgency=medium

  * Team upload
  * Add d/tests

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Sat, 09 Feb 2019 07:30:32 +0530

autosize.js (4.0.2~dfsg1-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Team upload
  * Reupload to unstable
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Add Breaks for ruby-rails-assets-autosize (<< 4.0)

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Thu, 07 Feb 2019 16:09:16 +0530

autosize.js (4.0.2~dfsg1-1) experimental; urgency=medium

  * New upstream version 4.0.2~dfsg1
  * Bump Standards-Version to 4.1.5 (no changes needed)
  * Bump debhelper compatibility level to 11
  * Build dist/autosize.js with babeljs

 -- Pirate Praveen <praveen@debian.org>  Mon, 23 Jul 2018 17:32:42 +0530

autosize.js (3.0.21~dfsg1-2) unstable; urgency=medium

  * Move to salsa.debian.org.
  * Priority extra -> optional.

 -- Alexandre Viau <aviau@debian.org>  Sat, 30 Dec 2017 15:44:26 -0500

autosize.js (3.0.21~dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version to 4.0.0.

 -- Alexandre Viau <aviau@debian.org>  Tue, 04 Jul 2017 15:06:14 -0400

autosize.js (3.0.15~dfsg1-1) unstable; urgency=medium

  * New upstream version
  * Bumped Standards-Version to 3.9.7
  * Changed Vcs-Git and Vcs-Browser to secure (https) urls

 -- Alexandre Viau <aviau@debian.org>  Wed, 23 Mar 2016 11:57:04 -0400

autosize.js (3.0.14~dfsg1-1) unstable; urgency=medium

  * Initial release. (Closes: #809364)

 -- Alexandre Viau <aviau@debian.org>  Tue, 29 Dec 2015 15:52:13 -0500
